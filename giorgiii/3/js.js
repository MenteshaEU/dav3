function countSubstrings(str, substr) {
    let count = 0;
    let position = str.indexOf(substr);

    while (position !== -1) {
        count++;
        position = str.indexOf(substr, position + 1);
    }

    return count;
}

let string = "ეს ტექსტი შედის შედეგებში რამდენიმე ჯერ";
let substring = "შე";
document.write(countSubstrings(string, substring));
document.write("<hr>")