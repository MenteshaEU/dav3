function countCharacterA(str) {
    let count = 0;
    for (let i = 0; i < str.length; i++) {
        if (str[i] === 'a' || str[i] === 'A') {
            count++;
        }
    }
    return count;
}

const sampleString = "";
document.write("სიმბოლო 'a'-ს რაოდენობა:", countCharacterA(sampleString));
document.write("<hr>")
