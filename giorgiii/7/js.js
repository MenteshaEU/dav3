function countSubstrings(str, substr) {
    const regex = new RegExp(substr, 'g');
    const matches = str.match(regex);
    return matches ? matches.length : 0;
}
const str = "JavaScript is an awesome programming language, JavaScript is also widely used.";
const substr = "JavaScript";
document.write(countSubstrings(str, substr)); 
document.write("<hr>")
  