function RandomString(n) {
    const alphabet = "abcdefghijklmnopqrstuvwxyz";
    let result = '';
    for (let i = 0; i < 20; i++) {
      for (let j = 0; j < n; j++) {
        const randomIndex = Math.floor(Math.random() * alphabet.length);
        result += alphabet[randomIndex];
      }
      result += ' ';
    }
    return result.trim(); 
  }
  
  const n = 5;
  const randomString = RandomString(n);
  document.write(randomString);
  document.write("<hr>")
